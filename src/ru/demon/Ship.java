package ru.demon;

import java.util.ArrayList;

/*
*класс описывающий объект корабль
*
 */
public class Ship {
    public final String THEY_ALREADY_SHOT_HERE = "Вы уже стреляли в это место";
    public final String GOT = "Вы попали!";
    public final String MISS = "Промах";
    public final String DESTROYED = "Корабль потоплен";
    private ArrayList coordinateShip = new ArrayList();
    private ArrayList coordinateShot = new ArrayList();

    /*
    *конструктор кораблика
    *
    */
    Ship(int sizeChannel, int sizeShip) {
        int a = (int) (Math.random() * (sizeChannel - sizeShip));
        for (int i = 1; i <= sizeShip; i++) {
            coordinateShip.add(a);
            a++;
        }
    }

    /*
    *конструктор по умолчанию
    *
     */
    Ship() {
        this(12, 3);
    }

    /*
    *метод сообщающий о состояний корабля
    *
    * @return true если уничтожен, false если жив
    */
    void statusShip(int shot) {
        int x = coordinateShip.indexOf(shot);
        if (coordinateShot.contains(shot)) {
            System.out.println(THEY_ALREADY_SHOT_HERE);
        }
        else if  (!(-1 == x)) {

            coordinateShip.remove(x);
            System.out.println(GOT);
        } else {
            System.out.println(MISS);
        }
        coordinateShot.add(shot);
    }

    /*
    *метод проверяющий уничтожен ли корабль
    *
    */
    boolean isDestroyed() {
        return coordinateShip.isEmpty();
    }
}

