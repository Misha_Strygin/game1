package ru.demon;

import java.util.Scanner;

/*
*Главный клас позволяющий поиграть в адмирала и потопить вражеский корабль
*
*
 */
public class Game {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите размер канала");
        int sizeChannel = scanner.nextInt();
        while (sizeChannel < 2) {
            System.out.println("Вы ввели размер канала куда не помещается корабль, попробуйте ввести размер заного");
            sizeChannel = scanner.nextInt();
        }

        System.out.println("Введите размер корабля");
        int sizeShip = scanner.nextInt();
        while (sizeShip < 1 || sizeShip > sizeChannel) {
            System.out.println("Размер корабля должен быть больше 1 клетки и меньше размера канала");
            sizeShip = scanner.nextInt();
        }
        Ship ship = new Ship(sizeChannel, sizeShip);
        sizeChannel++;
        countAndShooting(sizeChannel, ship);
    }

    /*
    *Метод счетающий количество выстрелов пользователя и реализующий саму стрельбу
    *
    * @return количество выстрелов
     */
    public static void countAndShooting(int sizeChannel, Ship ship) {
        int count = 0;
        do {
            System.out.println("Введите координату места куда будете стрелять");
            int shot = scanner.nextInt();
            while (shot < 0 || shot >= sizeChannel) {
                System.out.println("Пробуйте ввести число которое, будет больше 0 и меньше или равно длинне канала");
                shot = scanner.nextInt();
            }
            ship.statusShip(shot);
            count++;
        } while (!ship.isDestroyed());
        System.out.println("Вы уничожили корабль за " + count + " попыток");
    }
}

